# Ubuntu

## Todo

- [ ] Document the default tools in `sbin`
- [ ] Check out AutoJump: https://olivierlacan.com/posts/cd-is-wasting-your-time/


## Terminal Shortcut

`Ctrl+Alt+T`

## Screen Settings

*System Settings* > *Brightness and Lock* > *Turn screen off when inactive for* select *Never*

## Update Everything

```sh
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get dist-upgrade
```

## Spotify

[Install instructors](https://www.spotify.com/en/download/linux/)

`snap install spotify`

## Keeping Secrets

See [Secret Management](https://bloggo.herokuapp.com/secret-management).

## [A minimalist guide to tmux](https://medium.com/actualize-network/a-minimalist-guide-to-tmux-13675fb160fa) by [Peter Jang](https://medium.com/@peterxjang)

`tmux`

- `Ctrl+b` + `c`: Create a new window (appears in status bar)
- `Ctrl+b` + `#`: Switch to window # (0-N)
- `Ctrl+b` + `x`: Kill current window
- `Ctrl+b` + `d`: Detach tmux (exit back to normal terminal)

`Ctrl+b` is a *prefix*. Let go of it before pressing the command key.

```sh
tmux ls # List all sessions
tmux a # Attach last used session
tmux a -t <session-name>
```

There is also a `tmux.conf` file.

## [Moving efficiently in the CLI](https://clementc.github.io/blog/2018/01/25/moving_cli/) by [Clément Chastagnol](https://clementc.github.io/pages/about.html)

- [Unicode Box Drawing](http://jrgraphix.net/r/Unicode/2500-257F)
- [Unicode Arrows](https://unicode-table.com/en/sets/arrows-symbols/)

```
  ← Ctrl+A               Ctrl+E →  ┓
  ┌────────────┬────────────────┐  ┃
  │  ← Alt+B   │   Alt+F →      │  ┃ Moving
  │  ┌─────────┼─────────┐      │  ┃
  │  │ Ctrl+B ┌┼┐ Ctrl+F │      │  ┃
  ↓  ↓        ↓│↓        ↓      ↓  ┛
$ cp everything-everything music/
  ↑  └─────────┼──────────┘     ↑  ┓
  │  ← Ctrl+W  │    Alt+D →     │  ┃ Erasing
  └────────────┴────────────────┘  ┃
  ← Ctrl+D               Ctrl+K →  ┛
```

Everything but `Ctrl+D` works on WSL.
Additionally, `Ctrl+→` and `Ctrl+←` works there as expected.
`Home` and `End` to as well.
`Esc+Backspace` removes one word back.

## Default tools in `bin`

Based on https://partner-images.canonical.com/core/xenial/current/

- `bash` is the the GNU Bourne-Again Shell
- `cat` is for piping files to output
- `cp` is for copying files
- `dash` is the Debian Almquist Shell (this one is the default I think)
- `date` print formatted current date and time
- `dd` is for copying files with some encoding conversions
- `df` returns the amount of available disk space, `df -h` for human readable
- `dir` is like `ls -C -b`, that is, by default files are listed in columns, sorted vertically, and special characters are represented by backslash escape sequences
- `dmesg` kernel ring buffer tool - useful for debugging?
- `dnsdomainname` displays computers DNS thing
- `domainname` see [Understanding the Domainname Command](https://www.ibm.com/developerworks/community/blogs/55eba574-33c9-4600-a0dd-ad7aad16bb5e/entry/Understanding_the_Domainname_Command_in_Linux?lang=en)
- `egrep` is `grep -E`
- `echo` echoes
- `false` returns exit code 1
- `fgrep` is `grep -F`
- `findmnt` displays a tree of file systems
- `fuser` seems to be useful for finding what user/process(?) accesses what sockets/files
- `fusermount` unmount a FUSE file system
- `getfacl` get file ACL
- `grep` is just grep
- `gunzip` is like `gzip -d` I guess
- `gzexe` compresses executable files in place so they are smaller but start slower
- `gzip` is for zipping things
- `hostname` see [Understanding the Domainname Command](https://www.ibm.com/developerworks/community/blogs/55eba574-33c9-4600-a0dd-ad7aad16bb5e/entry/Understanding_the_Domainname_Command_in_Linux?lang=en)
- `chacl` changes ACLs
- `chgrp` changes group
- `chmod` is mostly `chmod +x file`
- `chown` is chown
- `ip` show or manage routing
- `journalctl` queries SystemD journal (log)
- `kill` sends `TERM` to a process
- `ln` makes links (different from Windows shortcuts, which are just files - these are file system objects)
- `login` invoked automatically on startup
- `loginctl` SystemD login manager
- `lowntfs-3g` NTFS driver
- `ls` is `ls`
- `lsblk` lists block devices (doesn't work on WSL)
- `mkdir` makes a directory
- `mknod` makes a block or character special file
- `mktemp` makes a temp file and prints its name
- `more` pages through text files
- `mount` mounts
- `mountpoint` shows if a directory is a mount point
- `mv` moves files and directories
- `nano` edits text files
- `networkctl` controls SystemD network daemon
- `nisdomainname` see [Understanding the Domainname Command](https://www.ibm.com/developerworks/community/blogs/55eba574-33c9-4600-a0dd-ad7aad16bb5e/entry/Understanding_the_Domainname_Command_in_Linux?lang=en)
- `ntfs-3g` is `lowntfs-3g`
- `ntfscat` shows files and streams on an NTFS device
- `ntfscluster` NTFS stuff
- `ntfs*`
- `pidof` gives PIDs of programs by name, like `pidof bash`
- `ps` lists current processes
- `pwd` prints working directory path
- `rbash` is `bash -r`,. restricted bash
- `readlink` translates a link to a canonical path
- `rm` removes
- `rmdir` removes a directory
- `rnano` restricted `nano`, cool :-)
- `run-parts` run all executables matching a pattern
- `sed` good ol' stream editor
- `setfacl` set file ACLs
- `sh` seems to be an alias for `dash` on Ubuntu, huh
- `sh.distrib` ?
- `sleep` sleeps, potentially for long, probably not by `while(true)`ing then
- `ss` displays socket statistics
- `stty` terminal characteristics
- `su` become super user
- `sync` flushes to disk - not sure why this is a thing
- `system*` SystemD stuff
- `tailf` use `tail -f` instead
- `tar` tarring files
- `tempfile` another temp file print path thing - why two?
- `touch` basically "create empty file" for all intents and purposes (or update `a`)
- `true` returns exit code 0
- `udevadm` is udev manager, what is udev?
- `ulockmgr_server` FUSE lock manager
- `umount` unmount
- `uname` system info, `uname -a` for all
- `uncompress` is `gzip -u`
- `vdir` vertical variant of `dir`
- `vmmouse_detect` VMWare thing (?!), exit code 0 if mouse, 1 if nouse
- `wdctl` what is hardware watchdog?
- `which` where dat binary at
- `ypdomainname` see [Understanding the Domainname Command](https://www.ibm.com/developerworks/community/blogs/55eba574-33c9-4600-a0dd-ad7aad16bb5e/entry/Understanding_the_Domainname_Command_in_Linux?lang=en)
- `zcat` `cat` for GZIP?
- `zcmp` compare compressed files
- `zdiff` diff compressed files
- `z*` more GZIP+sth stuff

- [ ] Figure out what sort of workflow `dmesg` enhances
